use std::fs

pub struct File {
    path: String,

}

impl Emu {
    pub fn new() -> Self {

    }

    pub fn clone() {

    }

    pub fn copy() {

    }

    pub fn move() {

    }

    pub fn rename() {

    }

    pub fn delete() {

    }

    pub fn createDir() {

    }

    pub fn changePermissions() {

    }

    pub fn changeOwner() {

    }

    pub fn listDir() {

    }

    pub fn metadata() {
        
    }

}
